# Aide et ressources de Larch pour Synchrotron SOLEIL

[<img src="https://xraypy.github.io/xraylarch/_static/larchcones.png" width="250"/>](https://xraypy.github.io/xraylarch/#)

## Résumé

- Logiciel basé sur python pour le traitement des données XAS
- Open source

## Sources

- Code source: https://github.com/xraypy/xraylarch
- Documentation officielle: https://xraypy.github.io/xraylarch/

## Navigation rapide

| Tutoriaux | Ressources annexes |
| - | - |
| [Tutoriel d'installation officiel](https://xraypy.github.io/xraylarch/installation.html) | [Mailing list](https://millenia.cars.aps.anl.gov/mailman/listinfo/ifeffit/) |
| [Tutoriaux officiels](https://xraypy.github.io/xraylarch/#contents) | |

## Installation

- Systèmes d'exploitation supportés: Linux, MacOS
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: n'importe quel type de fichier importable en python
- en sortie: tout type d'export python
- sur un disque dur, sur la Ruche
